{
  "title": "Valhalla Cannon",
  "date": "2019-12-01T12:41:05-05:00",
  "image": "/img/valhalla2.png",
  "link": "https://github.com/GastaGaming/ValhallaCannon/",
  "description": "Wacky mobile game where you solve puzzles by shooting various vikings out of a cannon.",
  "tags": ["Urho3D","Game","Project","Mobile","C++"],
  "fact": "",
  "featured":true
}
Wacky mobile game where you solve puzzles by shooting various vikings out of a cannon.
![asd](/img/valhalla.png)
{{< vid valhallabird.mp4>}}

Some of my main responsibilities in this project:

- Player controls for aiming, shooting and switching vikings.

- Basic flying and collision mechanics for vikings and obstacles.

- Core game loop functionality (starting sequence -> win condition -> next level)

- Helper tool for assembling and positioning obstacles in the levels.