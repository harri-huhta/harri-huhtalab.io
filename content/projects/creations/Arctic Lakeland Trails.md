{
  "title": "Arctic Lakeland Trails",
  "date": "2020-08-01T12:41:05-05:00",
  "image": "/img/outdoors.png",
  "description": "A collection of simple nature-themed minigames we made for a local travel app.",
  "tags": ["Unity","Game","Project","Mobile","Android","C#"],
  "fact": "",
  "featured":true
}
A collection of simple nature-themed minigames we made for [a local travel app](https://play.google.com/store/apps/details?id=fi.kamkhub.arcticlakelandkainuu).

{{< yt a_VVOVBRwO0 >}}

Some of my main responsibilities in the project:  
- Coding basically all of the gameplay as the only programmer in the game-team
- Testing and implementing an [Unity-React](https://github.com/f111fei/react-native-unity-view) system that allowed us to embed our Unity game into an existing travel app made with React Native
- Simple localization system capable of swapping text translations during runtime
- Resolution tracker that helps acommodate game elements based on the users screen aspect ratio.

