{
  "title": "RootOut",
  "date": "2018-09-30T12:41:05-05:00",
  "image": "/img/rootout.png",
  "link": "https://github.com/Dreadfang0/B2SJam",
  "description": "Silly PvP game we made during the 2018 'back to school' -gamejam.",
  "tags": ["Unity","Game","Jam","C#"],
  "fact": "",
  "featured":true
}
Silly local multiplayer PvP game we made during the 2018 "back to school" -gamejam.  
Random themes of the jam: "root" and "loot".
{{< vid rootout.mp4>}}

My main responsibility during this jam was creating the endless level generator.