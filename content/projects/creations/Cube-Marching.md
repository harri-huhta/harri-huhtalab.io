{
  "title": "Cube Marching",
  "date": "2018-12-11T12:41:05-05:00",
  "image": "/img/cubeMarch.png",
  "description": "A tech demo I made showcasing mesh generation through cube-marching and field generation within Unity.",
  "tags": ["Unity","Tech","Demo","C#"],
  "fact": "",
  "featured":true
}
A barebones tech demo I made showcasing mesh generation through cube-marching and field generation within Unity.  
Based on Paul Bourke's ["Polygonising a scalar field"](http://paulbourke.net/geometry/polygonise/) from 1994.

Digging around, altering the distance field to modify the mesh.
{{< vid diggin.mp4>}}

Closeup of the field grid used for mesh creation.
{{< vid meshGen2.mp4>}}

Here is the field generation hooked into audio sampling info from music.
{{< vid musicCut.mp4>}}

And here are some cave structures.
{{< vid caves.mp4>}}