{
  "title": "Augur of Light",
  "date": "2020-06-01T12:41:05-05:00",
  "image": "/img/augur.png",
  "link": "https://kim-k1mpp4-kupiainen.itch.io/augur-of-light",
  "description": "Atmospheric puzzle game based in Finnish nature.",
  "tags": ["Unity","Game","Project","C#"],
  "fact": "",
  "featured":true
}
Atmospheric puzzle game based in Finnish nature.

{{< yt YwalVTJV5LY >}}

Some of my main responsibilities in the project:  

- Creating a custom save & load system for saving & loading gameobject hierarchies, positions and component & script states.  

- Creating a scene management system that handles asynchronously loading and unloading parts of our multi-scene setup.  
Our game world is huge and quite open so distance based loading and multiple LODs were utilized to improve performance.

- Creating a footstep system that samples the terrain below the players feet, and categorizes the type of terrain from the its dominant texture.  
The type of terrain is then used to play different sounds depending on what terrain the player is walking on (eg. different sounds when walking on grass, sand, gravel, shallow water).

- Creating an asynchronous loading screen with visible loading progress and a decorative 3D-model display.  
