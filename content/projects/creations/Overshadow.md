{
  "title": "Overshadow",
  "date": "2019-06-01T12:41:05-05:00",
  "image": "/img/Overshadow3.png",
  "description": "Dark 2D sidescroller with simplistic controls.",
  "tags": ["Unity","Game","Project","C#"],
  "fact": "",
  "featured":true
}

Dark 2D sidescroller with simplistic controls.
{{< yt -ZoxB2NrXOM >}}

My main responsibilities in the project were coding the character movements and combat system for the player & NPCs.