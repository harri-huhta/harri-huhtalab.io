{
  "title": "VR Quidditch",
  "date": "2018-12-01T12:41:05-05:00",
  "image": "/img/quidditch.png",
  "description": "A fun little VR demo we created back in 2018 during our first tech workshop at Kajaani University of Applied Sciences.",
  "tags": ["Unity","Project","VR","Tech","Demo","C#"],
  "fact": "",
  "featured":true
}
{{< yt zIrDnBH6y0I >}}

A fun little VR demo we created back in 2018 during our first tech workshop at Kajaani University of Applied Sciences.

My responsibility was to program the flight controls, and flying logic for the snitch.