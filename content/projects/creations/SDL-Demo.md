{
  "title": "SDL Multiplayer demo",
  "date": "2017-12-01T12:41:05-05:00",
  "image": "/img/sdl.png",
  "description": "Small demo trying out SDL",
  "tags": ["SDL2","Tech","Demo","C++","SQL"],
  "fact": "",
  "featured":true
}

{{<vid sdl.mp4>}}

Small demo of a solo project I made for creating a basic component system in C++, implementing A* pathfinding in it and replicating the game over the internet.  
Made utilizing the SDL2 framework for basics such as rendering, input, and basic UDP interface.

[Technical-Design-Document](/sdl_tdd.pdf)