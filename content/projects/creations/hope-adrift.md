{
  "title": "Hope Adrift",
  "date": "2019-08-01T12:41:05-05:00",
  "image": "/img/adrift3.png",
  "link": "https://robo-dogo.itch.io/hope-adrift",
  "description": "Beautiful but simple action game about surviving on a broken steamboat and holding off hordes of robotic fish!",
  "tags": ["Unity","Game","Project","C#"],
  "fact": "",
  "featured":true
}
Beautiful and simple shooter game we made during the summer of 2019.


![asd](/img/adrift.png)  
___Your ships engine has mysteriously exploded and you are adrift at sea. Your only way of fixing it is to gather spare parts from the mechanical fish swimming below while you are trying to uncover the mystery of who was responsible for your dire situation.___


{{<yt 3ijWKPuqelQ>}}

Some of my main responsibilities in the project:  
- Fish behaviour
  - movement, swarming behaviour
  - [coordination system that calculates the travel time for each fish in a swarm and queues them for a synchronized attack.](/vid/hopeadrift_queue.mp4)  
- Spawner system for the fishes.
- Tooltip / highlight features

Check out our producers devlog of Hope Adrift at https://www.youtube.com/playlist?list=PL5eqG5zyIGB53Kb7R8Mn0bynEUJuFG2UJ
