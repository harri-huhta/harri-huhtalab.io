---
title: "Home"
date: 2018-02-10T18:56:13-05:00
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
Game programmer currently studying at Kajaani University of Applied Sciences.

Most proficient in Unity, C# and C++, but always interested in learning more about new platforms and workflows.

Started first experimenting with game development back in junior high school and have been hooked since.